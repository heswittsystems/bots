import undetected_chromedriver
import sys
from selenium.webdriver.common.action_chains import ActionChains
from PIL import Image
from selenium.webdriver.common.keys import Keys
import time

#specify chromedriver version to download and patch
undetected_chromedriver.TARGET_VERSION = 80

# or specify your own chromedriver binary to patch
undetected_chromedriver.install()

from selenium.webdriver import Chrome, ChromeOptions
opts = ChromeOptions()
#profile = {"plugins.plugins_disabled": ["Shockwave Flash"]}
#opts.add_experimental_option("prefs", profile)
opts.add_argument(f'--remote-debugging-port=9222')
opts.add_argument(f'--no-sandbox')
#proxy = "41.78.26.155:8080"
#opts.add_argument('--proxy-server=%s' % proxy)

#opts.add_argument(f'--disable-plugins-discovery')
#opts.add_argument(f'--disable-internal-flash')
opts.add_argument(f'--disable-bundled-ppapi-flash')
opts.add_argument(f'--user-agent=Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>')
driver = Chrome(options=opts)
driver.set_window_size(1400,1000)
#driver.get('https://distilnetworks.com')
#driver.get('http://dogeclick.com/visit/MeSYAqW')
url = str(sys.argv[1])
print(url)
driver.get(url)
time.sleep(10)
src = driver.page_source
fl = open(f'/var/www/html/out.html','w')
fl.write(src)
fl.close()
if src.find('Please solve the puzzle to continue'):
    print('Captcha found')
    element = driver.find_element_by_xpath("//div[@class='mt-2 d-flex justify-content-center']")
    location = element.location
    size = element.size
    driver.save_screenshot("/var/www/html/image.png")
    x = 152
    y = 225
    width = 449
    height = 356 
    im = Image.open('/var/www/html/image.png')
    im = im.crop((int(x), int(y), int(width), int(height)))
    im.save('/var/www/html/image2.png')
    driver.quit
    sys.exit(2)
driver.quit
