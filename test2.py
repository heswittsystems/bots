import undetected_chromedriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

#specify chromedriver version to download and patch
undetected_chromedriver.TARGET_VERSION = 80

# or specify your own chromedriver binary to patch
undetected_chromedriver.install()

from selenium.webdriver import Chrome, ChromeOptions
opts = ChromeOptions()
#profile = {"plugins.plugins_disabled": ["Shockwave Flash"]}
#opts.add_experimental_option("prefs", profile)
opts.add_argument(f'--remote-debugging-port=9222')
opts.add_argument(f'--no-sandbox')
#proxy = "41.78.26.155:8080"
#opts.add_argument('--proxy-server=%s' % proxy)

#opts.add_argument(f'--disable-plugins-discovery')
#opts.add_argument(f'--disable-internal-flash')
opts.add_argument(f'--disable-bundled-ppapi-flash')
opts.add_argument(f'--user-agent=Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>')
opts.add_experimental_option( "prefs",{'profile.managed_default_content_settings.javascript': 2})
driver = Chrome(options=opts)
driver.set_window_size(1400,1000)
#driver.get('https://distilnetworks.com')
driver.get('https://www.kilimall.co.ke/new/commoditysearch?q=books')
from selenium.webdriver.common.by import By
#wait = WebDriverWait(driver, 20)
#h3 = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "showHand")))

#page = browser.get(url)
#inner_html = driver.execute_script("return document.body.innerHTML")
#html = driver.execute_script("return document.body.outerHTML;")
html = driver.find_element_by_tag_name('html').get_attribute('innerHTML')
time.sleep(10)
print(html)
src = driver.page_source
fl = open(f'/var/www/html/out.html','w')
fl.write(src)
fl.close()
driver.quit
