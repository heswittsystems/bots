import undetected_chromedriver
import wget
import requests
import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import sys
from selenium.webdriver.common.action_chains import ActionChains
from PIL import Image
from selenium.webdriver.common.keys import Keys
import time
from twocaptchaapi import TwoCaptchaApi

#specify chromedriver version to download and patch
undetected_chromedriver.TARGET_VERSION = 80

# or specify your own chromedriver binary to patch
undetected_chromedriver.install()

from selenium.webdriver import Chrome, ChromeOptions
opts = ChromeOptions()
#profile = {"plugins.plugins_disabled": ["Shockwave Flash"]}
#opts.add_experimental_option("prefs", profile)
opts.add_argument(f'--remote-debugging-port=9222')
opts.add_argument(f'--no-sandbox')
proxy = "socks5://127.0.0.1:9050"
#opts.add_argument('--proxy-server=%s' % proxy)

#opts.add_argument(f'--disable-plugins-discovery')
#opts.add_argument(f'--disable-internal-flash')
opts.add_experimental_option("prefs", {
        "download.default_directory": "/home/ice/",
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
})

opts.add_experimental_option("excludeSwitches", ["enable-automation"])
opts.add_experimental_option('useAutomationExtension', False)
opts.add_argument(f'--disable-bundled-ppapi-flash')
#opts.add_argument(f'--start-maximized')
ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
opts.add_argument(f'--user-agent={ua}')
driver = Chrome(options=opts)
driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
  "source": """
    Object.defineProperty(navigator, 'webdriver', {
      get: () => undefined
    })
  """
})
#driver.set_window_size(1400,1000)
#url = 'https://distilnetworks.com'
#driver.get('http://dogeclick.com/visit/MeSYAqW')
url = "https://www.google.com/recaptcha/api2/demo"
#url = "https://www.ping.eu"
#url = "https://whatsmyip.com"
#str(sys.argv[1])
print(url)
driver.delete_all_cookies()

driver.get(url)

time.sleep(20)
src = driver.page_source
fl = open(f'/var/www/html/out.html','w')
fl.write(src)
fl.close()
driver.save_screenshot("/var/www/html/image2.png")
#driver.quit()
#quit()
if src.find('robot'):
    print('Captcha found')
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR,"iframe[name^='a-'][src^='https://www.google.com/recaptcha/api2/anchor?']")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "span#recaptcha-anchor"))).click()
    driver.switch_to.default_content()
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR,"iframe[title='recaptcha challenge']")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button#recaptcha-audio-button"))).click()
    if src.find('download audio as MP3'):
        print('Link for dl found')
    driver.save_screenshot("/var/www/html/image.png")
    #WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "a.rc-audio-challenge-tdownload-link"))).click()
    elems = driver.find_elements_by_xpath("//a[@href]")
    for elem in elems:
        link = elem.get_attribute("href")
        print(link)
        #wget.download(link, 'dl.mp3')

         
        headers = {'Accept':'*/*','Accept-Encoding':'gzip,deflate,sdch','Accept-Language':'en-US,en;q=0.8','Connection':'keep-alive','Referer':'http://www.google.com/recaptcha/demo/','user-agent': ua}
        myfile = requests.get(link,headers=headers)
        fname = 'cap' + '.mp3'
        outname = fname + '.flac'
        open(fname , 'wb').write(myfile.content)
        cmd = f'sox {fname} {outname}'
        stream = os.popen(cmd)
        output = stream.read()
        cmd = f'php /home/ice/transcribe/speech/src/transcribe_sync.php {outname}'
        stream = os.popen(cmd)
        output = stream.read()
        print(output)
        #driver.quit()
        #quit()
        text_area = driver.find_element_by_id('audio-response')
        text_area.send_keys(output)
        text_area.send_keys(Keys.ENTER)
        time.sleep(10)
        driver.save_screenshot("/var/www/html/image3.png")
        

    """
    element = driver.find_element_by_xpath("//div[@class='mt-2 d-flex justify-content-center']")
    location = element.location
    size = element.size
    driver.save_screenshot("/var/www/html/image.png")
    x = 152
    y = 225
    width = 449
    height = 356 
    im = Image.open('/var/www/html/image.png')
    im = im.crop((int(x), int(y), int(width), int(height)))
    im.save('/var/www/html/image2.png')
    api = TwoCaptchaApi('7a8102d2bf0a569cca2295f148529ddb')
    with open('/var/www/html/image2.png', 'rb') as captcha_file:
        captcha = api.solve(captcha_file)
        print(captcha.await_result())
    """    
    driver.quit
    sys.exit(2)
driver.quit
