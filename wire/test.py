import seleniumwire
from seleniumwire import webdriver  # Import from seleniumwire

# Create a new instance of the Firefox driver
#driver = webdriver.Chrome()
opts = webdriver.ChromeOptions()
#profile = {"plugins.plugins_disabled": ["Shockwave Flash"]}
#opts.add_experimental_option("prefs", profile)
opts.add_argument(f'--remote-debugging-port=9222')
opts.add_argument(f'--no-sandbox')
#proxy = "41.78.26.155:8080"
#opts.add_argument('--proxy-server=%s' % proxy)

#opts.add_argument(f'--disable-plugins-discovery')
#opts.add_argument(f'--disable-internal-flash')
opts.add_argument(f'--disable-bundled-ppapi-flash')
opts.add_argument(f'--user-agent=Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>')
driver = webdriver.Chrome(options=opts)
# Go to the Google home page
driver.get('https://www.google.com')

# Access requests via the `requests` attribute
for request in driver.requests:
    if request.response:
        print(
            request.path,
            request.response.status_code,
            request.response.headers['Content-Type']
        )
